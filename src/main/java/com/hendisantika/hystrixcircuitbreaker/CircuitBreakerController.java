package com.hendisantika.hystrixcircuitbreaker;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : hystrix-circuit-breaker
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/05/21
 * Time: 21.31
 */
@RestController
public class CircuitBreakerController {

    @GetMapping("/hello")
    @HystrixCommand(fallbackMethod = "fallbackMethod")
    public String getHelloWorld() {
        if (RandomUtils.nextBoolean())
            throw new RuntimeException("");
        return "Hello World!";
    }

    private String fallbackMethod() {
        return "fallback method called !";
    }

}
